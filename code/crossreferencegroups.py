import json

def extendRe3data(r3key, gruppo):
    global r3dic
    global roardic 
    global externalRefs
    foundErrors = False
    if r3key in r3dic['opendoar']:
        odkey = r3dic['opendoar'][r3key]
        if odkey in externalRefs:
            errors.write("ERROR Opendoar %s already inserted in previous set (ref %s)\n " % (odkey, r3key))
            foundErrors = True
        gruppo.add(odkey)
        externalRefs.add(odkey)
    if r3key in r3dic['roar']:
        rrkey = r3dic['roar'][r3key]
        if rrkey in externalRefs:
            errors.write("ERROR roar %s already inserted in previous set (ref %s ) \n" %(rrkey, r3key))
            foundErrors = True
        gruppo.add(rrkey)
        externalRefs.add(rrkey)
        if rrkey in roardic:
            od = roardic[rrkey]
            if not od in gruppo and od in externalRefs:
                errors.write("ERROR Opendoar %s already inserted in previous set (ref %s -> %s)\n " %(od, r3key, rrkey))
                foundErrors = True
            gruppo.add(od)
            externalRefs.add(od)
    return foundErrors

fairPrefix = 'fairsharing_::'
r3Prefix = 're3data_____::'
odoarPrefix = 'opendoar____::'
roarPrefix = 'roar________::'

#creation of the crossreference structure for fairsharing and of the correspondence fairsharing doi -> fairsharing id
fin = open('./data/in/fairsharing_dump_api_02_2022.json')
cref = {}
fairdic = {}
for line in fin:
    dic = json.loads(line)
    if 'cross-references' in dic['attributes']['metadata']:
            re3data = [e['name'] for e in dic['attributes']['metadata']['cross-references'] if e['portal']=='re3data']
            if len(re3data) > 0 :
                cref[fairPrefix + dic['id']] = r3Prefix + re3data[0][8:]
    if not dic['attributes']['doi'] is None:
        fairdic[dic['attributes']['doi'].lower()] = dic['id']

fin.close()


#creation of the re3data crossrefence structure
fin = open('./data/in/re3data.tsv')
l = fin.readline()
r3dic= {'fairsharing' : {}, 'opendoar' : {}, 'roar' : {}}
for line in fin:
    tmp = line.split('\t')
    if tmp[5] != '[]':
        if 'fairsharing' in tmp[5].lower() or 'opendoar' in tmp[5].lower() or 'roar' in tmp[5].lower():
            ids = json.loads(tmp[5])
            for i in ids:
                if 'fairsharing_doi' in i.lower():
                    key = i[16:].strip().lower()
                    if key[0] == ':':
                        key = key[1:]
                    if not key in fairdic:
                        print("%s => %s\n"%(tmp[0],key))
                    else:
                        r3dic['fairsharing'][r3Prefix + tmp[0]] = fairPrefix + fairdic[key]
                    continue
                if 'opendoar' in i.lower():
                    r3dic['opendoar'][r3Prefix + tmp[0]] = odoarPrefix + i[9:].strip()
                    continue
                if 'roar' in i.lower():
                    r3dic['roar'][r3Prefix + tmp[0]] = roarPrefix + i[5:].strip()

fin.close()

#FAIRSHARING doi missing (adding them manually) 
# r3d100010415 => 10.25504/fairsharing.fj07xj -> 1220
r3dic['fairsharing'][r3Prefix + 'r3d100010415'] = fairPrefix + '1220'
# r3d100010422 => 10.25504/fairsharing.7b0fc3 -> 1091
r3dic['fairsharing'][r3Prefix + 'r3d100010422'] = fairPrefix + '1091'
# r3d100012120 => 10.25504/fairsharing.3kfn3j -> 362
r3dic['fairsharing'][r3Prefix + 'r3d100012120'] = fairPrefix + '362'
# r3d100012626 => 10.25504/fairsharing.62qk8w -> 1036
r3dic['fairsharing'][r3Prefix + 'r3d100012626'] = fairPrefix + '1036'
# r3d100013199 => 10.24404/fairsharing.xb3lbl -> 2876
r3dic['fairsharing'][r3Prefix + 'r3d100013199'] = fairPrefix + '2876'
# r3d100013295 => 10.25504/fairsharing.wp3t2 -> 2679 (typo. miss L at the end of the doi)
r3dic['fairsharing'][r3Prefix + 'r3d100013295'] = fairPrefix + '2679'

# this is needed because in the input data there is the url
r3dic['roar']['re3data_____::r3d100013273'] = roarPrefix + '14208'

#creation of the roar crossreference structure
fin = open('./data/in/roarCrossRef_1.tsv')

fin.readline()
roardic = {}
for line in fin:
    line = line.split('\t')
    if line[1].lower() == 'opendoar':
        if line[2].strip() != "":
            roardic[roarPrefix +line[0].strip()] = odoarPrefix + line[2].strip()

fin.close()

#creation of groups from the crossreferences among the registries 
registryGroups = []
toCheckGroups = []
externalRefs = set()

errors = open('./data/out/ErrorsCrossRefs.txt','w')

#searching crossreferences from fairsharing (only versus re3data)
for fkey in cref:    
    foundErrors = False
    r3key = cref[fkey]
    gruppo = set()
    gruppo.add(fkey)
    gruppo.add(r3key)
    externalRefs.add(r3key)
    externalRefs.add(fkey)
    #verifies that re3data references back the same fairsharing id (if a crossreference from re3data to fairsharing exists)
    #continues anyway to create the group
    if r3key in r3dic['fairsharing'] and not r3dic['fairsharing'][r3key] == fkey:
        errors.write("ERROR %s -> %s -> %s\n"%(fkey, r3key, r3dic['fairsharing'][r3key]))
        foundErrors = True
    if not foundErrors and not extendRe3data(r3key, gruppo):
        registryGroups.append(gruppo)
    else:
        toCheckGroups.append(gruppo)


#searching cross references from re3data not already considered before

#first the fairsharing remained from the fairsharing check
for r3key in r3dic['fairsharing']:
    foundErrors = False
    if r3key in externalRefs:
        continue 
    fkey =  r3dic['fairsharing'][r3key]
    if fkey in externalRefs:
        errors.write ("ERROR %s -> %s -> %s\n"%(r3key, fkey, cref[fkey]))
        foundErrors = True 
    gruppo = set()
    gruppo.add(r3key)
    gruppo.add(fkey)
    externalRefs.add(r3key)
    externalRefs.add(fkey)
    if not foundErrors and not extendRe3data(r3key, gruppo):
        registryGroups.append(gruppo)
    else:
        toCheckGroups.append(gruppo)

#second the roar 
for r3key in r3dic['roar']:
    foundErrors = False 
    if r3key in externalRefs:
        continue 
    rrkey = r3dic['roar'][r3key]
    if rrkey  in externalRefs:
        errors.write("ERROR roar %s already inserted in previous set (ref %s)\n " %(rrkey, r3key))
        foundErrors = True
    gruppo = set()
    gruppo.add(r3key)
    gruppo.add(rrkey)
    externalRefs.add(r3key)
    externalRefs.add(rrkey)
    if r3key in r3dic['opendoar']:
        odkey = r3dic['opendoar'][r3key]
        if odkey in externalRefs:
            errors.write("ERROR Opendoar %s already inserted in previous set (ref %s)\n " %(odkey, r3key))
            foundErrors = True
        gruppo.add(odkey)
        externalRefs.add(odkey)
        if rrkey in roardic:
            od = roardic[rrkey]
            if not od in gruppo and od in externalRefs:
                errors.write("ERROR Opendoar %s already inserted in previous set (ref %s -> %s)\n " %(od, r3key, rrkey))
                foundErrors = True
            gruppo.add(od)
            externalRefs.add(od)
    if not foundErrors:
        registryGroups.append(gruppo)
    else:
        toCheckGroups.append(gruppo)

#last those remaining from re3data to opendoar
for r3key in r3dic['opendoar']:
    foundErrors = False
    if r3key in externalRefs:
        continue 
    odkey = r3dic['opendoar'][r3key]
    if odkey in externalRefs:
        errors.write("ERROR opendoar %s already inserted in previous set (ref %s) " %(odkey, r3key))
        foundErrors = True
    gruppo = set()
    gruppo.add(r3key)
    gruppo.add(odkey)
    externalRefs.add(odkey)
    if not foundErrors:
        registryGroups.append(gruppo)
    else:
        toCheckGroups.append(gruppo)


#searching crossreference from roar not already considered
for rrkey in roardic:
    foundErrors = False
    if rrkey in externalRefs:
        continue
    odkey = roardic[rrkey]
    if odkey in externalRefs:
        #here we have to check if the previous formed group has to be extended with roar
        for g in registryGroups:
            if odkey in g:
                g.add(rrkey)
                externalRefs.add(rrkey)
                break
        continue
    gruppo = set()
    gruppo.add(rrkey)
    gruppo.add(odkey)
    externalRefs.add(rrkey)
    externalRefs.add(odkey)
    if not foundErrors:
        registryGroups.append(gruppo)
    else:
        toCheckGroups.append(gruppo)


errors.close()

fout = open('./data/out/registryGroups.txt','w')
for g in registryGroups:
    fout.write(json.dumps(list(g)) + "\n")

fout.close()


fout = open('./data/out/toCheckGroups.txt','w')
for g in toCheckGroups:
    fout.write(json.dumps(list(g)) + "\n")

fout.close()

fout = open('./data/out/report_registry_groups.txt','w')
fout.write("Fairsharing references to re3data = %s\n"%len(cref))
fout.write("Re3Data references to Fairsharing = %s\n"%len(r3dic['fairsharing']))
fout.write("Re3Data references to opendoar = %s\n"%len(r3dic['opendoar']))
fout.write("Re3Data references to roar = %s\n"%len(r3dic['roar']))
fout.write("ROAR references to opendoar = %s\n"%len(roardic))
fout.close()
